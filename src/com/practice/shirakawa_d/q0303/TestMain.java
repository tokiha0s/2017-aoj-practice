package com.practice.shirakawa_d.q0303;

import java.util.Arrays;

import org.junit.Test;

public class TestMain {
    @Test
    public void testMain() throws Exception {
        Main.main(null);
    }

    @Test
    public void test() throws Exception {
        char[][] grid = {
                {
                        '.',
                        '.',
                        '.',
                        '1',
                        '4',
                        '.',
                        '.',
                        '2',
                        '.' },
                {
                        '.',
                        '.',
                        '6',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.' },
                {
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.' },
                {
                        '.',
                        '.',
                        '1',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.' },
                {
                        '.',
                        '6',
                        '7',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '9' },
                {
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '8',
                        '1',
                        '.' },
                {
                        '.',
                        '3',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '6' },
                {
                        '.',
                        '.',
                        '.',
                        '.',
                        '.',
                        '7',
                        '.',
                        '.',
                        '.' },
                {
                        '.',
                        '.',
                        '.',
                        '5',
                        '.',
                        '.',
                        '.',
                        '7',
                        '.' } };
        System.out.println(sudoku2(grid));
    }

    boolean sudoku2(char[][] grid) {
        String[][] note = makeNote(grid);
        return removeDuplicateNumber(note);
    }

    private String[][] makeNote(char[][] grid) {
        String def = "123456789";

        String[][] note = new String[9][];
        for (int i = 0; i < 9; i++) {
            note[i] = new String[9];
            Arrays.fill(note[i], def);
        }

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (grid[i][j] != '.') {
                    note[i][j] = String.valueOf(grid[i][j]);
                }
            }
        }

        return note;
    }

    private boolean removeDuplicateNumber(String[][] note) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (note[i][j].length() == 1 && !removeOf(note, i, j)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean removeOf(String[][] note, int i, int j) {
        String targetChar = note[i][j];

        for (int k = 0; k < 9; k++) {
            if (j == k) {
                continue;
            }

            // horizontal check
            if (note[i][k].contains(targetChar)) {
                note[i][k] = note[i][k].replaceAll(targetChar, "");
                if (note[i][k].isEmpty() || (note[i][k].length() == 1 && !removeOf(note, i, k))) {
                    return false;
                }
            }

            // vertical check
            if (note[k][i].contains(targetChar)) {
                note[k][i] = note[k][i].replaceAll(targetChar, "");
                if (note[k][i].isEmpty() || note[k][i].length() == 1 && !removeOf(note, k, i)) {
                    return false;
                }
            }

            // block check
            int bi = ((i / 3) * 3) + (k / 3);
            int bj = (j / 3) + (k / 3);
            if (note[bi][bj].contains(targetChar)) {
                note[bi][bj] = note[bi][bj].replaceAll(targetChar, "");
                if (note[bi][bj].isEmpty() || note[bi][bj].length() == 1 && !removeOf(note, bi, bj)) {
                    return false;
                }
            }
        }

        return true;
    }

}
