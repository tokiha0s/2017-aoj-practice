package com.practice.shirakawa_d.q0303;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Main {

    public static void main(String[] args) throws Exception {
        List<Chikaramochi> ckrmcList = createList();

        int walkingCkrmcCount = analyzeMinimum(ckrmcList);

        System.out.println(walkingCkrmcCount);
    }

    private static List<Chikaramochi> createList() {
        try (Scanner sc = new Scanner(new InputStreamReader(System.in))) {
            int ckrmcCount = sc.nextInt();

            List<Chikaramochi> list = new ArrayList<>();
            for (int i = 0; i < ckrmcCount; i++) {
                int weight = sc.nextInt();
                int carry = sc.nextInt();
                list.add(new Chikaramochi(weight, carry));
            }

            return list;
        }
    }

    private static int analyzeMinimum(List<Chikaramochi> ckrmcList) {
        while (0 < carryBestChikaramochi(ckrmcList)) {
            //
        }
        return ckrmcList.size();
    }

    private static int carryBestChikaramochi(List<Chikaramochi> ckrmcList) {
        int maxCarriable = -1;
        int bestCkrmcIndex = -1;
        boolean isLeft = false;

        for (int i = 0; i < ckrmcList.size(); i++) {
            int carriableLeft = countCarriableLeft(ckrmcList, i);
            int carriableRight = countCarriableRight(ckrmcList, i);

            if (maxCarriable < carriableLeft) {
                maxCarriable = carriableLeft;
                bestCkrmcIndex = i;
                isLeft = true;
            }

            if (maxCarriable < carriableRight) {
                maxCarriable = carriableRight;
                bestCkrmcIndex = i;
                isLeft = false;
            }
        }

        carryBestCkrmc(ckrmcList, maxCarriable, bestCkrmcIndex, isLeft);
        return maxCarriable;
    }

    private static void carryBestCkrmc(List<Chikaramochi> ckrmcList, int maxCarriable, int bestCkrmcIndex,
            boolean isLeft) {
        Chikaramochi bestCkrmc = ckrmcList.get(bestCkrmcIndex);
        for (int i = 1; i <= maxCarriable; i++) {
            int a = (isLeft) ? -i : 1;
            Chikaramochi ckrmc = ckrmcList.remove(bestCkrmcIndex + a);
            bestCkrmc.carringWeight += ckrmc.weight + ckrmc.carringWeight;
        }
    }

    private static int countCarriableLeft(List<Chikaramochi> ckrmcList, int targetCkrmcIdx) {
        Chikaramochi ckrmc = ckrmcList.get(targetCkrmcIdx);

        int tmpWeight = 0;
        int carriable = 0;
        for (int iLeft = targetCkrmcIdx - 1; 0 <= iLeft; iLeft--) {
            int tmp = ckrmcList.get(iLeft).getWeight();
            if ((tmpWeight + tmp) <= ckrmc.power) {
                carriable++;
                tmpWeight += tmp;
            } else {
                break;
            }
        }
        return carriable;
    }

    private static int countCarriableRight(List<Chikaramochi> ckrmcList, int targetCkrmcIdx) {
        Chikaramochi ckrmc = ckrmcList.get(targetCkrmcIdx);

        int tmpWeight = 0;
        int carriable = 0;
        for (int iRight = targetCkrmcIdx + 1; iRight < ckrmcList.size(); iRight++) {
            int tmp = ckrmcList.get(iRight).getWeight();
            if ((tmpWeight + tmp) <= ckrmc.power) {
                carriable++;
                tmpWeight += tmp;
            } else {
                break;
            }
        }
        return carriable;
    }

    public static class Chikaramochi {
        public final int weight;
        public final int power;

        public int carringWeight = 0;

        public Chikaramochi(int weight, int power) {
            super();
            this.weight = weight;
            this.power = power;
        }

        public int getWeight() {
            return weight + carringWeight;
        }
    }
}
