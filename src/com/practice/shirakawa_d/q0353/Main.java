package com.practice.shirakawa_d.q0353;

import java.io.InputStreamReader;
import java.util.Scanner;

class Main {

    public static void main(String[] args) throws Exception {
        Link[] array = createLinkedArray();

        int swapCount = sortAndCount(array);

        System.out.println(swapCount);
    }

    private static Link[] createLinkedArray() {
        try (Scanner sc = new Scanner(new InputStreamReader(System.in))) {
            final int arraySize = sc.nextInt();
            final Link[] array = new Link[arraySize];

            // 前後の要素をindexのつながりで表す簡易LinkedListを構築
            for (int i = 0; i < array.length; i++) {
                final int value = sc.nextInt();
                final int prevIndex = i - 1;
                final int nextIndex = i + 1;

                array[i] = new Link(value, i, prevIndex, nextIndex);
            }

            // 始端/終端は-1で表す
            array[0].prevIndex = -1;
            array[array.length - 1].nextIndex = -1;

            return array;
        }
    }

    public static int sortAndCount(Link[] array) {
        int swapCount = 0;
        Link target = array[1];
        Link last = array[array.length - 1];

        for (;;) {
            if (target.prevIndex == -1) {
                // targetが始端なら１つ進める
                target = array[target.nextIndex];
                continue;
            }

            Link previous = array[target.prevIndex];

            if (previous.value < target.value) {
                if (target.nextIndex == -1) {
                    // 終端までチェックしたら終了
                    break;
                }
                // previous のほうが小さければ targetを１つ進める
                target = array[target.nextIndex];
                continue;
            }

            // targetを前へ詰める
            if (previous.prevIndex != -1) {
                array[previous.prevIndex].nextIndex = target.index;
            }
            target.prevIndex = previous.prevIndex;

            // previousを終端に付け替える
            last.nextIndex = previous.index;
            previous.prevIndex = last.index;
            previous.nextIndex = -1;
            last = previous;

            swapCount++;
        }

        return swapCount;
    }

    public static class Link {
        public final int value;
        public final int index;

        public int prevIndex;
        public int nextIndex;

        public Link(int value, int index, int prevIndex, int nextIndex) {
            this.value = value;
            this.index = index;
            this.prevIndex = prevIndex;
            this.nextIndex = nextIndex;
        }

        @Override
        public String toString() {
            return String.valueOf("[" + index + "]" + value);
        }
    }

}
