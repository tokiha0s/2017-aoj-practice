package com.practice.shirakawa_d.q0353;

import org.junit.Test;

import com.practice.shirakawa_d.q0353.Main.Link;

public class TestMain {
    @Test
    public void testMain() throws Exception {
        Main.main(null);
    }

    @Test
    public void testName() throws Exception {
        Link[] array = generateArray(200000);
        int swapCount = Main.sortAndCount(array);
        System.out.println(swapCount);
    }

    private Link[] generateArray(int size) {
        Link[] array = new Link[size];

        for (int i = 0; i < size; i++) {
            int value = size - i;
            array[i] = new Link(value, i, i - 1, i + 1);
        }

        array[0].prevIndex = -1;
        array[size - 1].nextIndex = -1;

        return array;
    }
}
