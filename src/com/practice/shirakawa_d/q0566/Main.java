package com.practice.shirakawa_d.q0566;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

class Main {

    private static final int WIN_POINT = 3;
    private static final int EVEN_POINT = 1;
    private static final int LOSE_POINT = 0;

    public static void main(String[] args) throws Exception {
        List<SoccerTeam> teams = readGames();

        List<SoccerTeam> result = ranking(teams);

        for (SoccerTeam team : result) {
            System.out.println(team.getRank());
        }
    }

    private static List<SoccerTeam> readGames() {
        try (Scanner sc = new Scanner(new InputStreamReader(System.in))) {
            int teamCount = sc.nextInt();

            List<SoccerTeam> teams = new ArrayList<>(teamCount);
            for (int i = 0; i < teamCount; i++) {
                teams.add(new SoccerTeam(i + 1));
            }

            while (sc.hasNext()) {
                SoccerTeam teamA = teams.get(sc.nextInt() - 1);
                SoccerTeam teamB = teams.get(sc.nextInt() - 1);
                int scoreA = sc.nextInt();
                int scoreB = sc.nextInt();

                if (scoreA == scoreB) {
                    teamA.add(EVEN_POINT);
                    teamB.add(EVEN_POINT);
                } else if (scoreA < scoreB) {
                    teamA.add(LOSE_POINT);
                    teamB.add(WIN_POINT);
                } else {
                    teamA.add(WIN_POINT);
                    teamB.add(LOSE_POINT);
                }
            }

            return teams;
        }
    }

    private static List<SoccerTeam> ranking(List<SoccerTeam> teams) {
        // sort by point
        List<SoccerTeam> sorted = teams.stream()
                .sorted(Comparator
                        .comparing(SoccerTeam::getPoint)
                        .reversed())
                .collect(Collectors.toList());

        // set rank
        int rank = 1;
        SoccerTeam lastRanked = null;

        for (SoccerTeam team : sorted) {
            if (Objects.nonNull(lastRanked)
                    && lastRanked.getPoint() == team.getPoint()) {
                // set same rank for teams that have same points
                team.setRank(lastRanked.getRank());
            } else {
                team.setRank(rank);
                lastRanked = team;
            }
            rank++;
        }

        sorted.sort(Comparator.comparing(SoccerTeam::getTeamNo));
        return sorted;
    }

    static class SoccerTeam {
        private final int teamNo;
        private int point = 0;
        private int rank;

        public SoccerTeam(int teamNo) {
            this.teamNo = teamNo;
        }

        public void add(int point) {
            this.point += point;
        }

        public int getTeamNo() {
            return teamNo;
        }

        public int getPoint() {
            return point;
        }

        public void setRank(int rank) {
            this.rank = rank;
        }

        public int getRank() {
            return rank;
        }
    }
}
